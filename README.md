![picture](https://cdn2.steamgriddb.com/logo_thumb/fe8c15fed5f808006ce95eddb7366e35.png) 

The Legend of Zelda: Majora's Mask Powered by the 2s2h reimplimentation engine 
This has no affiliation with the open source 2s2h reimplimentation engine.

Repository for AUR package
Packages:
[2s2h-bin](https://aur.archlinux.org/packages/2s2h-bin))
[zeldamm](https://aur.archlinux.org/packages/zeldamm))

 ### Author
  * Corey Bruce
